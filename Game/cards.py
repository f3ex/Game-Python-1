# -*- coding: utf-8 -*-
from stuff import *
import random


class Card(object):
    def __init__(self, level=0):
        self.__level = level


class StuffCard(Card):

    def get_item(self):
        stuff_tuple = (EmptyStuff, HeadStuff, BodyStuff, LegStuff, WeaponStuff, ShieldStuff)
        return random.choice(stuff_tuple)(self.__level)


class GoldCard(Card):
    def __init__(self, _=0):
        self.money = random.choice(100, 100, 100, 200, 200, 400)

    def get_item(self):
        return self.money


def get_cards(count):
    cards_tuple = (StuffCard, GoldCard, GoldCard)
    if count > 0:
        cards_list = [random.choice(cards_tuple) for _ in range(0, count)]
        return cards_list
    else:
        return []
