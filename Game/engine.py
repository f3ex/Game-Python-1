# -*- coding: utf-8 -*-

from player import Player
from game_map import Map


class Engine(object):

    def __init__(self):
        self.player = Player()
        self.game_map = Map()

    def goodbye(self):
        print "\nBye.\n"
        exit(0)

    def died(self):
        print "*" * 24
        print "YOU LOSE!"
        print self.player.info()
        print "*" * 24
        exit(1)

    def win(self):
        print "*" * 24
        print "YOU WIN!\nCONGRATULATES!!!"
        print self.player.info()
        print "*" * 24
        exit(0)

    def play_room(self):
        current_room = self.game_map.get_room()
        print current_room.info
        current_room.play(self.player)
        if self.player.level >= 5:
            self.win()

    def inventory_controller(self):
        self.player.work_with_inventory()

    def get_room_info(self):
        current_room = self.game_map.get_room()
        print current_room.info

    def show_map(self):
        self.game_map.print_map()

    def prompt(self):
        # print "Choose action:"
        print
        print "1 => Show player info | ",
        print "2 => Get information about room"
        print "3 => Show the map | ",
        print "4 => Go to the inventory"
        print "7 => Move up | ",
        print "8 => Move down | ",
        print "9 => Move left | ",
        print "0 => Move right"

        try:
            action = int(raw_input("> "))
            return action
        except EOFError:
            self.goodbye()
        except Exception:
            return None

    def play(self):
        try:
            while True:
                if self.player.hp > 0:
                    action = self.prompt()
                    if action == 1:
                        print self.player.info()
                    elif action == 2:
                        self.get_room_info()
                    elif action == 3:
                        self.show_map()
                    elif action == 4:
                        self.inventory_controller()
                    elif action == 7:
                        self.game_map.move_up()
                        self.play_room()
                    elif action == 8:
                        self.game_map.move_down()
                        self.play_room()
                    elif action == 9:
                        self.game_map.move_left()
                        self.play_room()
                    elif action == 0:
                        self.game_map.move_right()
                        self.play_room()
                    else:
                        print "Bad action"
                        continue
                else:
                    self.died()
        except KeyboardInterrupt:
            self.goodbye()