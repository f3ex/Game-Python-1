#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from engine import Engine


def main():
    game = Engine()
    game.play()


if __name__ == "__main__":
    main()