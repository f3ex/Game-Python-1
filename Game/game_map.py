# -*- coding: utf-8 -*-

import room


class Map(object):

    map_size = 5

    def __init__(self):
        self.game_map = []
        # generate map 5x5
        for i in range(0, self.map_size):
            self.game_map.append([])
            self.game_map[i] = [None for _ in range(0, self.map_size)]

        # Default position (2;2) -> Center of Map
        self.x = 2
        self.y = 2

        # Set init room
        self.game_map[self.x][self.y] = room.EnterRoom()

        self.print_map()

    def get_room_info(self):
        current_room = self.get_room()
        print current_room.info

    def get_room(self):
        return self.game_map[self.x][self.y]

    def __open_room(self):
        if not self.game_map[self.x][self.y]:
            self.game_map[self.x][self.y] = room.get_room()

    def print_map(self):
        for i in range(0, self.map_size):
            for j in range(0, self.map_size):
                if self.x == i and self.y == j:
                    print " x  ",
                else:
                    room_var = self.game_map[i][j]
                    if isinstance(room_var, room.GoldRoom):
                        print "Gold",
                    elif isinstance(room_var, room.TrapRoom):
                        print "Trap",
                    elif isinstance(room_var, room.EnterRoom):
                        print "Cntr",
                    elif isinstance(room_var, room.MonsterRoom):
                        print "Mnst",
                    else:
                        print "Room",
                    # print self.game_map[i][j],
            print
        print "*" * 32

    def move_up(self):
        if self.x == 0:
            self.x = len(self.game_map) - 1
        else:
            self.x -= 1
        self.__open_room()

    def move_down(self):
        if self.x == len(self.game_map) - 1:
            self.x = 0
        else:
            self.x += 1
        self.__open_room()

    def move_left(self):
        if self.y == 0:
            self.y = len(self.game_map[self.x]) - 1
        else:
            self.y -= 1
        self.__open_room()

    def move_right(self):
        if self.y == len(self.game_map[self.x]) - 1:
            self.y = 0
        else:
            self.y += 1
        self.__open_room()