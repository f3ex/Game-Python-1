# -*- coding: utf-8 -*-

import random


class Monster(object):

    def __init__(self, level=0, xp=0, gold_cars=0, name=""):
        self.level = level
        self.xp = xp
        self.gold_cards = gold_cars
        self.name = name


class Goblin(Monster):
    """Слабый юнит, дает мало карт"""
    def __init__(self, players_level):
        # Слабому игроку даем слабого монстра. Потом у игрока появляется какой-то шмот,
        # который дает игроку сил
        if players_level < 1:
            level = random.randrange(1, 3)
            gold_cards = 1
            xp = random.randrange(3, 8)
        else:
            level = random.randrange(1, 5) + players_level
            gold_cards = random.randrange(1, 3)
            xp = int(level/2) + 5
        super(Goblin, self).__init__(level, xp, gold_cards, "Goblin")


class Troll(Monster):
    """Сильный юнит, дает много карт. Должен встречаться только с третьего лвл"""
    def __init__(self, players_level):

        level = random.randrange(1, 15) + players_level
        gold_cards = random.randrange(1, 5)
        xp = int(level / 2) + 7
        super(Troll, self).__init__(level, xp, gold_cards, "Troll")


class Gazebo(Monster):
    """Средний монстр. Но это Газебо"""
    def __init__(self, players_level):

        level = random.randrange(1, 10) + players_level
        gold_cards = random.randrange(1, 3)
        xp = int(level / 2) + 4
        super(Gazebo, self).__init__(level, xp, gold_cards, "Gazebo")


monsters_list = (Goblin, Goblin, Goblin, Troll, Gazebo, Gazebo)


def get_random_monster(players_level):
    if players_level < 3:
        m = random.choice(monsters_list)
        while True:
            if not isinstance(m, Troll):
                return m
    else:
        return random.choice(monsters_list)

