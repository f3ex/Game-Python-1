# -*- coding: utf-8 -*-

from stuff import *


class Player(object):

    xp_map = {1: 10, 2: 20, 3: 40, 4: 80, 5: 80}

    def __init__(self):
        self.hp = 40
        # self.money = 100
        self.money = Gold(100)
        self.xp = 0
        self.level = 0
        self.inventory = Inventory()
        self.equipment = Equipment()

    def info(self):
        return "LVL: %s, HP: %s, XP: %s, Money %s" % (self.level, self.hp, self.xp, self.money)

    def update_xp(self, xp):
        self.xp += xp
        need_xp = self.xp_map[self.level+1]
        print "You got %s XP" % xp
        while self.xp > need_xp:
            self.level += 1
            self.xp -= need_xp
            if self.level < 5:
                print "Level updated! Need %d XP to next level" % self.xp_map[self.level + 1]
                need_xp = self.xp_map[self.level+1]
                # print need_xp
            else:
                break

    def work_with_inventory(self):
        def print_choice():
            print "1 => Show the inventory | ",
            print "2 => Show current equipments | ",
            print "3 => Wear armor | ",
            print "0 => Back to the map"
        while True:
            print_choice()
            try:
                action = int(raw_input("> "))
            except EOFError:
                print "Goodbye!"
                exit(1)
            except Exception:
                print "Bad answer."
                continue

            if action == 1:
                self.__show_inventory()
            elif action == 2:
                self.__show_equipment()
            elif action == 3:
                while True:
                    self.__show_inventory()
                    print "Write item's number"
                    try:
                        item_id = int(raw_input("> "))
                    except EOFError:
                        print "Goodbye!"
                        exit(1)
                    except:
                        print "Bad number"
                        continue
                    current_stuff = self.__delete_inventory(item_id)
                    current_stuff = self.__set_stuff(current_stuff)
                    if current_stuff:
                        self.__add_to_inventory(current_stuff)
                    break
            elif action == 0:
                return
            else:
                continue

    def __show_equipment(self):
        self.equipment.show_equipment()

    def __show_inventory(self):
        inventory = self.inventory.get_inventory()
        if len(inventory) == 0:
            print "Inventory is empty"
        else:
            for i in range(0, len(inventory)):
                print "%s => %s" % (i, inventory[i].info())

    def __set_stuff(self, stuff):
        current_stuff = self.equipment.set_stuff(stuff)
        if current_stuff:
            return current_stuff

    def __delete_inventory(self, stuff_id):
        return self.inventory.remove_stuff(stuff_id)

    def __add_to_inventory(self, stuff):
        self.inventory.add_stuff(stuff)

if __name__ == "__main__":
    cur_eq = []
    cur_eq.append(HeadStuff(name="Мега шлемак", additional_level=2))
    cur_eq.append(BodyStuff(name="Мега дупер нагрудник", additional_level=3))
    cur_eq.append(LegStuff(name="Вонючие носки", additional_level=1))
    cur_eq.append(WeaponStuff(name="Огненный меч", additional_level=2))

    player = Player()

    for stuff in cur_eq:
        player.equipment.set_stuff(stuff)

    player.inventory.add_stuff(HeadStuff(name="Шлемак из инвенторя", additional_level=2))

    player.work_with_inventory()
    print player.info()
    player.update_xp(40)
    print player.info()
    player.update_xp(500)
    print player.info()