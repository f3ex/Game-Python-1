# -*- coding: utf-8 -*-

from player import Player
import random
import monsters


class Room(object):
    opened = False
    info = ""
    type = 0
    pass


class GoldRoom(Room):

    def __init__(self):
        self.info = "This is a gold room"

    def play(self, player):
        if self.opened:
            print "The room have already been played"
        else:
            player.money.add(10)
            print "Gold Room, Money +10, Money: %s" % player.money
            self.opened = True


class TrapRoom(Room):

    def __init__(self):
        self.info =  "This is a trap room"

    def play(self, player):
        if self.opened:
            print "The room have already been played"
        else:
            player.hp -= 10
            print "Trap Room, hp -10, HP: %s" % player.hp
            self.opened = True


class EnterRoom(Room):

    def __init__(self):
        self.info = "This is an enter room"

    def play(self, player):
        if self.opened:
            print "It's empty room. Nothing to play here"
            self.opened = True


class MonsterRoom(Room):
    def __init__(self):
        self.info = "The monster is here!"

    def play(self, player):
        if self.opened:
            print "There are no monsters here."
        else:
            monster_class = monsters.get_random_monster(player.level)
            monster = monster_class(player.level)

            print "The %s (%s) is attacking you!" % (monster.name, monster.level)
            self.opened = True
            print "fighting will be released late"
            player.update_xp(monster.xp)

rooms_list = (MonsterRoom, MonsterRoom, MonsterRoom, MonsterRoom, TrapRoom, GoldRoom)


def get_room():
    r = random.choice(rooms_list)
    return r()