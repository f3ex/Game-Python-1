# -*- coding: utf-8 -*-


class Gold(object):
    def __init__(self, count=0):
        self.check_int(count)
        self.__count = count

    @property
    def money(self):
        return self.__count

    def add(self, count):
        self.check_int(count)
        self.__count += count

    def sub(self, count):
        self.check_int(count)
        if self.__count < count:
            self.__count = 0
        else:
            self.__count -= count

    def check_int(self, count):
        if type(count) != int:
            raise TypeError

    def __str__(self):
        return self.money



class Equipment(object):

    def __init__(self):
        self.head = EmptyStuff()
        self.body = EmptyStuff()
        self.leg = EmptyStuff()
        self.left_hand = EmptyStuff()
        self.right_hand = EmptyStuff()

    def set_stuff(self, stuff):
        if isinstance(stuff, HeadStuff):
            current_stuff = self.__get_head()
            self.__set_head(stuff)
            return current_stuff
        elif isinstance(stuff, BodyStuff):
            current_stuff = self.__get_body()
            self.__set_body(stuff)
            return current_stuff
        elif isinstance(stuff, LegStuff):
            current_stuff = self.__get_leg()
            self.__set_leg(stuff)
            return current_stuff
        elif isinstance(stuff, WeaponStuff):
            current_stuff = self.__get_weapon()
            self.__set_weapon(stuff)
            return current_stuff
        elif isinstance(stuff, ShieldStuff):
            current_stuff = self.__get_shield()
            self.__set_shield(stuff)
            return current_stuff
        else:
            return None

    def __get_head(self):
        return self.head

    def __get_body(self):
        return self.body

    def __get_leg(self):
        return self.leg

    def __get_weapon(self):
        return self.right_hand

    def __get_shield(self):
        return self.left_hand

    def __set_head(self, stuff):
        self.head = stuff

    def __set_body(self, stuff):
        self.body = stuff

    def __set_leg(self, stuff):
        self.leg = stuff

    def __set_weapon(self, stuff):
        self.right_hand = stuff

    def __set_shield(self, stuff):
        self.left_hand = stuff

    def show_equipment(self):
        print "Head: %s" % self.head.info()
        print "Body: %s" % self.body.info()
        print "Legs: %s" % self.leg.info()
        print "Weapon: %s" % self.right_hand.info()
        print "Shield: %s" % self.left_hand.info()


class Inventory(object):

    def __init__(self):
        self.inventory = []

    def get_inventory(self):
        return self.inventory

    def add_stuff(self, stuff):
        self.inventory.append(stuff)

    def remove_stuff(self, stuff_id):
        return self.inventory.pop(stuff_id)

    def size(self):
        return len(self.inventory)


class Stuff(object):

    def __init__(self, attack_count=0, defence_count=0, additional_level=0, need_user_level=0, name=None):
        self.attack_count = attack_count
        self.defence_count = defence_count
        self.additional_level = additional_level
        self.need_user_level = need_user_level
        self.name = name

    def __str__(self):
        return self.name

    def info(self):
        if self.name:
            return "%s (+%s)" % (self.name, self.additional_level)
        else:
            return None


class EmptyStuff(Stuff):
    pass


class HeadStuff(Stuff):
    pass


class BodyStuff(Stuff):
    pass


class LegStuff(Stuff):
    pass


class WeaponStuff(Stuff):
    pass


class ShieldStuff(Stuff):
    pass

