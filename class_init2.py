class MyClass(object):
    def __init__(self, a):
        print "Parent %s" % a


class Child(MyClass):
    pass


c = Child(123)