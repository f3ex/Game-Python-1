#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from abc import ABCMeta, abstractmethod, abstractproperty

class Movable(object):
    __metaclass__ = ABCMeta
    @abstractmethod
    def move(self):
        """Переместить объект"""
    @abstractproperty
    def speed(self):
        """Скорость объекта"""

class Car(Movable):
    def __init__(self):
        self.speed = 10
        self.x = 0
    def move(self):
        self.c += self.speed
    # def speed(self):
    #     return self.speed

car = Car()
car.speed = 100
print car.speed
#
# class Truck(Movable):
#     def move(self): pass
#     def speed(self): pass
#
# truck = Truck()