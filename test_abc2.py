#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from abc import ABCMeta, abstractmethod, abstractproperty

class Movable(object):
    __metaclass__ = ABCMeta

    @abstractproperty
    def speed(self):
        """Скорость объекта"""

class Car(Movable):

    def __init__(self):
        pass
    @property
    def speed(self):
        return 10


car = Car()
car.speed = 100
print car.speed