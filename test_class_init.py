class OneClass(object):

    def __init__(self, a):
        print "Parent, var: " + a


class TwoClass(OneClass):
    def __init__(self):
        super(TwoClass, self).__init__("two")
        print "Child"


two = TwoClass()