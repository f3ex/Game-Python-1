import random

class Player(object):

    def __init__(self):
        self.money = 0
        self.hp = 100

class Room(object):
    opened = False
    pass

class GoldRoom(Room):

    def play(self, player):
        player.money += 10
        print "Gold Room, Money +10, Money: %s" % player.money
        self.opened = True


class TrapRoom(Room):

    def play(self, player):
        player.hp -= 10
        print "Trap Room, hp -10, HP: %s" % player.hp
        self.opened = True

def get_room():
    r = random.randrange(0, 2)

    if r:
        return GoldRoom()
    else:
        return TrapRoom()


p = Player()
while True:
    print "Player inventory %s money, %s health" % (p.money, p.hp)
    if p.hp <= 0:
        print "You dead"
        exit(1)
    room = get_room()
    room.play(p)
